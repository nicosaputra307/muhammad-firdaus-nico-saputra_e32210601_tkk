import 'dart:convert';

import 'package:MAN_flutter/Components/Login/dialogs.dart';
import 'package:flutter/material.dart';
import 'package:MAN_flutter/Components/custom_surfix_icons.dart';
import 'package:MAN_flutter/Components/default_button_custom_color.dart';
import 'package:MAN_flutter/Screens/Dashboard/Dashboard.dart';
import 'package:MAN_flutter/Utils/constants.dart';
import 'package:MAN_flutter/size_config.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class SignInForm extends StatefulWidget {
  @override
  _SignInForm createState() => _SignInForm();
}

class _SignInForm extends State<SignInForm> {
  final _formKey = GlobalKey<FormState>();
  String? username;
  String? password;
  bool? remember = false;

  // TextEditingController txtUserName = TextEditingController(),
  //                       txtPassword = TextEditingController();

  FocusNode focusNode = new FocusNode();

  var emailController = TextEditingController();
  var passController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkLogin();
  }

  void checkLogin() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String? val = await pref.getString("login");
    if (val != null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => DashboardMain()),
          (route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        children: [
          buildUserName(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildPassword(),
          SizedBox(height: getProportionateScreenHeight(30)),
          Row(
            children: [
              Checkbox(
                  value: remember,
                  onChanged: (value) {
                    //Ketika checkbox di click value berubah menjadi true
                    setState(() {
                      remember =
                          value; //Yang tadi nya nilai false diubah oleh set state menjadi true
                    });
                  }),
              Text("Tetap Masuk"),
              Spacer(), //Mengatur jarak kanan kiri
              GestureDetector(
                onTap: () {},
                child: Text(
                  "Lupa Password",
                  style: TextStyle(decoration: TextDecoration.underline),
                ),
              )
            ],
          ),
          // SizedBox(
          //   height: 40,
          // ),
          // OutlinedButton.icon(
          //   onPressed: () {
          //     login();
          //   },
          //   icon: Icon(Icons.login),
          //   label: Text("Login"),
          // ),
          SizedBox(
            height: 40,
          ),
          DefaultButtonCustomeColor(
            color: kPrimaryColor,
            text: "LOGIN",
            press: () {
              login();
            },
          ),
          // DefaultButtonCustomeColor(
          //   color: kPrimaryColor,
          //   text: "LOGIN",
          //   press: () {
          //     Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
          //       return DashboardMain();
          //     }));
          //   },
          // ),
        ],
      ),
    );
  }

  TextFormField buildUserName() {
    return TextFormField(
      controller:
          emailController, //menampung nilai ketika memasukkan field username
      keyboardType: TextInputType.text,
      style: mTitleStyle,
      decoration: InputDecoration(
          labelText: 'Username',
          hintText: 'Masukkan username anda',
          labelStyle: TextStyle(
              color: focusNode.hasFocus ? mSubtitleColor : kPrimaryColor),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: CustomSurffixIcon(
            svgIcon: "assets/icons/Mail.svg",
          )),
    );
  }

  TextFormField buildPassword() {
    return TextFormField(
      controller:
          passController, // menampung nilai ketika memasukkan field username
      obscureText: true,
      style: mTitleStyle,
      decoration: InputDecoration(
          labelText: 'Password',
          hintText: 'Masukkan password anda',
          labelStyle: TextStyle(
              color: focusNode.hasFocus ? mSubtitleColor : kPrimaryColor),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: CustomSurffixIcon(
            svgIcon: "assets/icons/account.svg",
          )),
    );
  }

  void login() async {
    if (passController.text.isNotEmpty && emailController.text.isNotEmpty) {
      var response = await http.post(Uri.parse("https://reqres.in/api/login"),
          body: ({
            "email": emailController.text,
            "password": passController.text
          }));
      if (response.statusCode == 200) {
        final body = jsonDecode(response.body);
        print("Login Token" + body["token"]);
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text("Token: ${body['token']}")));
        pageRoute(body['token']);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text("Email atau Password Yang Anda Masukkan Salah")));
      }
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Kolom Tidak Boleh Kosong")));
    }
  }

  /* void _validateInputs() {
    if (_formKey.currentState!.validate()) {
      //If all data are correct then save data to out variables
      _formKey.currentState!.save();
      var txtEditPwd;
      var txtEditUsername;
      doLogin(txtEditUsername.text, txtEditPwd.text);
    }
  }

  doLogin(username, password) async {
    final GlobalKey<State> _keyLoader = GlobalKey<State>();

    try {
      final response =
          await http.post(Uri.parse("http://55.55.55.21/surat-desa/api/login"),
              headers: {'Content-Type': 'application/json; charset=UTF-8'},
              body: jsonEncode({
                "username": username,
                "password": password,
              }));

      final output = jsonDecode(response.body);
      debugPrint(output['data'].toString());
      debugPrint(response.statusCode.toString());
      debugPrint(response.toString());

      if (response.statusCode == 200) {
        Navigator.of(_keyLoader.currentContext!, rootNavigator: false).pop();
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
              content: Text(
            output['message'],
            style: const TextStyle(fontSize: 16),
          )),
        );

        if (output['success'] == true) {
          saveSession(output['data']);
        }
        //debugPrint(output['message']);
      } else {
        Navigator.of(_keyLoader.currentContext!, rootNavigator: false).pop();
        //debugPrint(output['message']);
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
              content: Text(
            output.toString(),
            style: const TextStyle(fontSize: 16),
          )),
        );
      }
    } catch (e) {
      Navigator.of(_keyLoader.currentContext!, rootNavigator: false).pop();
      Dialogs.popUp(context, '$e');
      debugPrint('$e');
    }
  }*/

  void pageRoute(String token) async {
    //menyimpan nilai token pada shared preferences
    SharedPreferences.setMockInitialValues({});
    SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.setString("login", token);
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => DashboardMain()),
        (route) => false);
  }

  /* void pageRoute(String token) async {
    //menyimpan nilai token pada shared preferences
    SharedPreferences.setMockInitialValues({});
    SharedPreferences pref = await SharedPreferences.getInstance();
    var islogin = pref.getBool("is_login");
    if (islogin != null && islogin) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => DashboardMain()),
          (route) => false);
    }
  }*/
}

void saveSession(output) {}
