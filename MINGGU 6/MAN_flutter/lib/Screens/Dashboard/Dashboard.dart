import 'package:flutter/material.dart';
import 'package:MAN_flutter/Components/Drawer/DrawerPage.dart';
import 'package:MAN_flutter/Screens/Login/LoginScreens.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DashboardMain extends StatefulWidget {
  @override
  State<DashboardMain> createState() => _DashboardMainState();
}

class _DashboardMainState extends State<DashboardMain> {
  String token = "";

  @override
  void initState() {
    super.initState();
    getCred();
  }

  void getCred() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      token = pref.getString("login")!;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Menantea App"),
      ),
      drawer: DrawerWidget(),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: SafeArea(
            child: Center(
                child: Column(
          children: [
            Text("Selamat Datang di Tampilan Dashboard"),
            SizedBox(
              height: 15,
            ),
            Text("Token Anda: $token"),
            SizedBox(
              height: 35,
            ),
            OutlinedButton.icon(
                onPressed: () async {
                  SharedPreferences pref =
                      await SharedPreferences.getInstance();
                  await pref.clear();
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(builder: (context) => LoginScreen()),
                      (route) => false);
                },
                icon: Icon(Icons.login),
                label: Text("Logout")),
          ],
        ))),
      ),
    );
  }
}

// class DashboardMain  extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Belajar Drawer Nav"),
//       ),
//       drawer: DrawerWidget(),
//       // endDrawer: DrawerWidget(),
//       body: Center(
//           child: Text('Ini Adalah Tampilan Dashboard',
//               style: TextStyle(fontSize: 20))),
//     );
//   }
// }