import 'package:flutter/material.dart';
import 'package:MAN_flutter/Screens/Login/LoginScreens.dart';

//Misal membuat screen baru harus didaftarkan di routes

final Map<String, WidgetBuilder> routes = {
  LoginScreen.routeName: (context) => LoginScreen()
};
