import 'package:flutter/material.dart';
import 'package:MAN_flutter/Screens/Login/LoginScreens.dart';
import 'package:MAN_flutter/routes.dart';
import 'package:MAN_flutter/theme.dart';

void main() async {
  runApp(MaterialApp(
    title: "Toko Menantea",
    theme: theme(),
    initialRoute: LoginScreen
        .routeName, //Class yang pertama kali dijalanin atau dipanggil
    routes: routes,
    debugShowCheckedModeBanner: false,
  ));
}
