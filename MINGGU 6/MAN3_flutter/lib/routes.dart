import 'package:flutter/material.dart';
import 'package:MAN3_flutter/Screens/Login/LoginScreens.dart';
import 'package:MAN3_flutter/Screens/Splash/SplashScreen.dart';
import 'package:splashscreen/splashscreen.dart';

//Misal membuat screen baru harus didaftarkan di routes

final Map<String, WidgetBuilder> routes = {
  SplashScreen().routeName: (context) => SplashMain()
};
