import 'package:flutter/material.dart';
import 'package:MAN3_flutter/Components/Drawer/DrawerPage.dart';
import 'package:MAN3_flutter/Components/Login/LoginForm.dart';
import 'package:MAN3_flutter/Screens/Login/LoginScreens.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DashboardMain extends StatefulWidget {
  @override
  State<DashboardMain> createState() => _DashboardMainState();
}

class _DashboardMainState extends State<DashboardMain> {
  String email = "";
  void getCred() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    var islogin = pref.getBool("is_login");
    if (islogin != null && islogin == true) {
      setState(() {
        email = pref.getString("email")!;
      });
    } else {
      Navigator.of(context, rootNavigator: true).pop();
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => SignInForm(),
        ),
        (route) => false,
      );
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCred();
  }

  @override
  dispose() {
    // TODO: implement initState
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("MAN 3 App"),
      ),
      drawer: DrawerWidget(),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: SafeArea(
            child: Center(
                child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Selamat Datang di Tampilan Dashboard"),
            SizedBox(
              height: 15,
            ),
            Text("Email Anda : " + email, style: TextStyle(fontSize: 20)),
            SizedBox(
              height: 35,
            ),
          ],
        ))),
      ),
    );
  }
}
