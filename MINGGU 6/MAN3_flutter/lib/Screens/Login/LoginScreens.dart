import 'package:flutter/material.dart';
import 'package:MAN3_flutter/Components/Login/LoginForm.dart';
import 'package:MAN3_flutter/size_config.dart';

class LoginScreen extends StatelessWidget {
  // static String routeName = "/sign_in";

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      // backgroundColor: Color(0xFFFFCC),
      appBar: AppBar(
        automaticallyImplyLeading: false,
      ),
      body: SignInForm(),
    );
  }
}
