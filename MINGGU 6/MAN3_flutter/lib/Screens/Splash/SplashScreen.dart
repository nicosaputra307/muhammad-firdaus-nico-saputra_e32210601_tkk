import 'dart:async';

import 'package:MAN3_flutter/Screens/Login/LoginScreens.dart';
import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';

class SplashMain extends StatefulWidget {
  const SplashMain({Key? key}) : super(key: key);

  static String routeName = "/screen_in";

  @override
  State<SplashMain> createState() => _SplashMainState();
}

class _SplashMainState extends State<SplashMain> {
  bool isVisible = false;

  @override
  void initState() {
    Timer(const Duration(seconds: 2), () {
      setState(() {
        isVisible = true;
      });
      toHomePage();
    });

    super.initState();
  }

  toHomePage() {
    Timer(const Duration(seconds: 3), () {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => LoginScreen()));
    });
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          AnimatedSwitcher(
            duration: const Duration(seconds: 2),
            transitionBuilder: (Widget child, Animation<double> animation) {
              return ScaleTransition(scale: animation, child: child);
            },
            child: isVisible
                ? FlutterLogo(size: screenSize.height / 4)
                : const SizedBox(),
          ),
          const SizedBox(height: 25),
          const Text("Selamat Datang di Earlerning MAN 3 KEDIRI",
              style: TextStyle(fontSize: 18)),
          isVisible
              ? const Align(
                  //alignment: FractionalOffset.bottomCenter,
                  heightFactor: 5,
                  alignment: Alignment.bottomCenter,
                  child: CircularProgressIndicator(
                    color: Colors.grey,
                  ))
              : const SizedBox()
        ],
      )),
    );
  }
}
