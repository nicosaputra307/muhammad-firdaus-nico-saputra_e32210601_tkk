import 'package:flutter/material.dart';
import 'package:MAN3_flutter/Screens/Login/LoginScreens.dart';
import 'package:MAN3_flutter/Screens/Splash/SplashScreen.dart';
import 'package:MAN3_flutter/routes.dart';
import 'package:MAN3_flutter/theme.dart';

void main() async {
  runApp(MaterialApp(
    title: "MAN 3 KEDIRI",
    theme: theme(),
    home: SplashMain(),
    debugShowCheckedModeBanner: false,
  ));
}
